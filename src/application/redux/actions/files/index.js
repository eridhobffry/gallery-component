import { ACTIONS } from "application/constants";

export const registerThumbnail = fileId => ({
    type: ACTIONS.FILES_REGISTER_THUMBNAIL,
    fileId
})

export const getThumbnailRequest = fileId => ({
    type: ACTIONS.FILES_GET_THUMBNAILS_REQUEST,
    fileId
})

export const getThumbnailFailed = fileId => ({
    type: ACTIONS.FILES_GET_THUMBNAILS_FAILED,
    fileId
})

export const getThumbnailSucceed = fileId => ({
    type: ACTIONS.FILES_GET_THUMBNAILS_SUCCEED,
    fileId
})

export const populateThumbnail = (fileId, payload) => ({
    type: ACTIONS.FILES_POPULATE_THUMBNAILS_REQUEST,
    fileId,
    payload
})

export const registerFullAttachment = fileId => ({
    type: ACTIONS.FILES_REGISTER_FULL_ATTACHMENT,
    fileId
})

export const getFullAttachmentRequest = fileId => ({
    type: ACTIONS.FILES_GET_FULL_ATTACHMENT_REQUEST,
    fileId
})

export const getFullAttachmentFailed = fileId => ({
    type: ACTIONS.FILES_GET_FULL_ATTACHMENT_FAILED,
    fileId
})

export const getFullAttachmentSucceed = fileId => ({
    type: ACTIONS.FILES_GET_FULL_ATTACHMENT_SUCCEED,
    fileId
})

export const populateFullAttachment = (fileId, payload) => ({
    type: ACTIONS.FILES_POPULATE_FULL_ATTACHMENT_REQUEST,
    fileId,
    payload
})