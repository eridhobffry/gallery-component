import { ACTIONS } from 'application/constants'

export const getUserDetailsRequest = (userId) => ({
    type: ACTIONS.USERS_GET_USER_DETAILS_REQUEST,
    userId
})

export const registerUser = (userId) => ({
    type: ACTIONS.USERS_REGISTER_USER_BY_ID,
    userId
})

export const getUserDetailsFailed = (userId) => ({
    type: ACTIONS.USERS_GET_USER_DETAILS_FAILED,
    userId
})

export const getUserDetailsSucceed = (userId) => ({
    type: ACTIONS.USERS_GET_USER_DETAILS_SUCCEED,
    userId
})

export const populateUserDetails = (userId, payload) => ({
    type: ACTIONS.USERS_POPULATE_USER_DETAILS,
    userId,
    payload
})