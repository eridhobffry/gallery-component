import { ACTIONS } from "application/constants"

export const showGallery = (fileId = null, fileName = '', thumbnails = []) => ({
    type: ACTIONS.GALLERY_SHOW_GALLERY,
    fileId,
    fileName,
    thumbnails,
})

export const setActiveFile = (fileId, fileName) => ({
    type: ACTIONS.GALLERY_SET_ACTIVE_FILE,
    fileId,
    fileName,
})

export const hideGallery = () => ({
    type: ACTIONS.GALLERY_HIDE_GALLERY
})