import { ACTIONS } from "../../../constants/actions";
import { DefaultStore } from "../default_store";

export const FilesReducer = (state = DefaultStore.files, action) => {
    switch (action.type) {
        case ACTIONS.FILES_REGISTER_THUMBNAIL:
            return {
                ...state,
                thumbnails: state.thumbnails.filter(t => t.id === action.fileId).length > 0 ? state.thumbnails
                    : [
                        ...state.thumbnails,
                        {
                            id: action.fileId,
                            request: true,
                            succeed: false,
                            failed: false,
                        }
                    ]
            }
        case ACTIONS.FILES_GET_THUMBNAILS_FAILED:
            return {
                ...state,
                thumbnails: state.thumbnails.map(s => s.id === action.fileId ? {
                    ...s,
                    request: false,
                    failed: true,
                } : s)
            }
        case ACTIONS.FILES_GET_THUMBNAILS_SUCCEED:
            return {
                ...state,
                thumbnails: state.thumbnails.map(s => s.id === action.fileId ? {
                    ...s,
                    request: false,
                    succeed: true,
                } : s)
            }
        case ACTIONS.FILES_POPULATE_THUMBNAILS_REQUEST:
            return {
                ...state,
                thumbnails: state.thumbnails.map(s => s.id === action.fileId ? {
                    ...s,
                    ...action.payload
                } : s)
            }
        case ACTIONS.FILES_REGISTER_FULL_ATTACHMENT:
            return {
                ...state,
                fullAttachments: state.fullAttachments.filter(t => t.id === action.fileId).length > 0 ? state.fullAttachments
                    : [
                        ...state.fullAttachments,
                        {
                            id: action.fileId,
                            request: true,
                            succeed: false,
                            failed: false,
                        }
                    ]
            }
        case ACTIONS.FILES_GET_FULL_ATTACHMENT_FAILED:
            return {
                ...state,
                fullAttachments: state.fullAttachments.map(s => s.id === action.fileId ? {
                    ...s,
                    request: false,
                    failed: true,
                } : s)
            }
        case ACTIONS.FILES_GET_FULL_ATTACHMENT_SUCCEED:
            return {
                ...state,
                fullAttachments: state.fullAttachments.map(s => s.id === action.fileId ? {
                    ...s,
                    request: false,
                    succeed: true,
                } : s)
            }
        case ACTIONS.FILES_POPULATE_FULL_ATTACHMENT_REQUEST:
            return {
                ...state,
                fullAttachments: state.fullAttachments.map(s => s.id === action.fileId ? {
                    ...s,
                    ...action.payload
                } : s)
            }
        default:
            return state
    }
}