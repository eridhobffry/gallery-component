export const DefaultStore = {
    files: {
        thumbnails: [],
        fullAttachments: [],
    },
    gallery: {
        isVisible: false,
        thumbnails: [],
        selectedId: null,
        selectedName: null
    },
    users: []
}