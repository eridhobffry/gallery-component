import { ACTIONS } from '../../../constants/actions';
import { DefaultStore } from '../default_store'

export const GalleryReducer = (state = DefaultStore.gallery, action) => {
    switch (action.type) {
        case ACTIONS.GALLERY_SHOW_GALLERY:
            return {
                ...state,
                isVisible: true,
                selectedId: action.fileId,
                selectedName: action.fileName,
                thumbnails: action.thumbnails,
            }
        case ACTIONS.GALLERY_SET_ACTIVE_FILE:
            return {
                ...state,
                selectedId: action.fileId,
                selectedName: action.fileName,
            }
        case ACTIONS.GALLERY_HIDE_GALLERY:
            return {
                ...state,
                isVisible: false,
                selectedId: null,
                selectedName: null,
                thumbnails: []
            }
        default:
            return state
    }
}

export default GalleryReducer
