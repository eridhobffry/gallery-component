import { ACTIONS } from '../../../constants/actions';
import { DefaultStore } from '../default_store'

export const UsersReducer = (state = DefaultStore.users, action) => {
    switch (action.type) {
        case ACTIONS.USERS_REGISTER_USER_BY_ID:
            return state.filter(t => t.id === action.userId).length > 0
                ? state
                : [
                    ...state,
                    {
                        id: action.userId,
                        request: true,
                        succeed: false,
                        failed: false,
                        payload: null,
                    }
                ]
        case ACTIONS.USERS_GET_USER_DETAILS_FAILED:
            return state.map(s => s.id === action.userId ? {
                ...s,
                request: false,
                failed: true,
            } : s )
        case ACTIONS.USERS_GET_USER_DETAILS_SUCCEED:
            return state.map(s => s.id === action.userId ? {
                ...s,
                request: false,
                succeed: true,
            } : s )
        case ACTIONS.USERS_POPULATE_USER_DETAILS:
            return state.map(s => s.id === action.userId ? {
                ...s,
                payload: action.payload
            } : s )
        default:
            return state
    }
}

export default UsersReducer
