import { combineReducers } from "redux";
import { FilesReducer } from "./files";
import GalleryReducer from "./gallery";
import { UsersReducer } from "./users";

const reducers = combineReducers({
    files: FilesReducer,
    gallery: GalleryReducer,
    users: UsersReducer
})

const appReducer = (state, action) => {
    return reducers(state, action)
}

export default appReducer