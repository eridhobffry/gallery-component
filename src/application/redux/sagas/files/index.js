
import { ACTIONS } from '../../../constants'
import { takeEvery, put, call, select } from 'redux-saga/effects'
import { getFullAttachmentFailed, getFullAttachmentSucceed, getThumbnailFailed, getThumbnailSucceed, populateFullAttachment, populateThumbnail } from 'application/redux/actions/files'
import { api } from 'application/api'
import { fullFilesLoaded, thumbnailLoaded } from 'application/redux/selectors'

function* getThumbnailSaga(action) {
    try {
        const isLoaded = yield select(thumbnailLoaded, action.fileId)
        if (!isLoaded) {
            const response = yield call(api.gateway.files.thumbnails.getThumbnailsById, action.fileId)
            yield put(populateThumbnail(action.fileId, response))
            yield put(getThumbnailSucceed(action.fileId))
        }
    } catch (error) {
        yield put(getThumbnailFailed(action.fileId))
    }
}

function* getFullAttachmentSaga(action) {
    try {
        const isLoaded = yield select(fullFilesLoaded, action.fileId)
        if (!isLoaded) {
            const response = yield call(api.gateway.files.fullAttachments.getFullFilesById, action.fileId)
            yield put(populateFullAttachment(action.fileId, response))
            yield put(getFullAttachmentSucceed(action.fileId))
        }
    } catch (error) {
        yield put(getFullAttachmentFailed(action.fileId))
    }
}

export function* filesWatcher() {
    yield takeEvery(ACTIONS.FILES_GET_THUMBNAILS_REQUEST, getThumbnailSaga)
    yield takeEvery(ACTIONS.FILES_GET_FULL_ATTACHMENT_REQUEST, getFullAttachmentSaga)
}