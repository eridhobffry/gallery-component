import { all } from 'redux-saga/effects'
import { filesWatcher } from './files'
import { usersWatcher } from './users'

export default function* rootSaga() {
    yield all([
        filesWatcher(),
        usersWatcher()
    ])
}