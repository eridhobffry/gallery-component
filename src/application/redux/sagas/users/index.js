import { api } from 'application/api'
import { getUserDetailsFailed, getUserDetailsSucceed, populateUserDetails } from 'application/redux/actions/users'
import { takeEvery, call, put } from 'redux-saga/effects'
import { ACTIONS } from '../../../constants'

function* getUserDetailsRequestSaga(action) {
    try {
        const userDetails = yield call(api.gateway.users.getUserDetailsById, action.userId)
        yield put(populateUserDetails(action.userId, userDetails))
        yield put(getUserDetailsSucceed(action.userId))
    } catch (error) {
        yield put(getUserDetailsFailed(action.userId))
    }
}

export function* usersWatcher() {
    yield takeEvery(ACTIONS.USERS_GET_USER_DETAILS_REQUEST, getUserDetailsRequestSaga)
}