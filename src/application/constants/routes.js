export const ROUTES = {
    BASE: '/',
    USERS: '/gallery/users',
    GALLERY_USERS_PRESELECTED: '/gallery/users/:userId',
}