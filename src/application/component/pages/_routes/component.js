import React from 'react'
import { Route, Switch, withRouter } from 'react-router'
import { Main, UserGallery, Users } from '..'
import { ROUTES } from '../../../constants/routes'

const RoutesComponent = () => {
    return <Switch>
        <Route exact path={ROUTES.BASE} component={Main} />
        <Route exact path={ROUTES.USERS} component={Users} />
        <Route exact path={ROUTES.GALLERY_USERS_PRESELECTED} component={UserGallery} />
    </Switch>
}

export default withRouter(RoutesComponent)
