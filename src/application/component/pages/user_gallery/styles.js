import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const UserGalleryContainer = styled.div`
    display: flex;
    padding: 60px;
    flex: 1 1 100%;
`

export const BackLink = styled(NavLink)`
    text-decoration: none;
    font-weight: ${p => p.theme.fontWeight.bold};
    font-size: ${p => p.theme.fontSize.small};
    color: ${p => p.theme.color.primary};
`

export const ContentWrapper = styled.div`
    display: flex;
    flex-flow: column nowrap;
    flex: 1 1 100%;
`

export const GalleryWrapper = styled.div`
    display: flex;
    padding: 10px;
    flex-flow: row wrap;
    flex: 0 0 auto;
    border: 3px solid black;
`