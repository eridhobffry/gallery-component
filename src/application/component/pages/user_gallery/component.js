import { ROUTES } from 'application/constants'
import React, { Fragment } from 'react'
import { BaseLayout } from '../_layout'
import UserDetailsLoader from './loader'
import * as Styled from './styles'
import Thumbnail from 'application/component/building_blocks/thumbnails'
import { nanoid } from 'nanoid'

const UserGalleryComponent = (props) => {
    const { userIsLoaded, userDetails, openGallery } = props
    const handleOpenGallery = (thumbDetails) => {
        openGallery(thumbDetails.id, thumbDetails.name, userDetails.gallery)
    }
    return <BaseLayout>
        <Styled.UserGalleryContainer>
            <UserDetailsLoader {...props}>
                {
                    userIsLoaded && <Fragment>
                        <Styled.ContentWrapper>
                            <Styled.BackLink to={ROUTES.USERS}>
                                BACK
                            </Styled.BackLink>
                            <h1>
                                {userDetails.firstName} {userDetails.lastName} Gallery
                            </h1>
                            <Styled.GalleryWrapper>
                                {
                                    userDetails.gallery.length > 0 && userDetails.gallery.map(g => {
                                        return <Thumbnail
                                            key={nanoid()}
                                            fileId={g.id}
                                            onThumbnailClick={(thumbDetails) => handleOpenGallery(thumbDetails)}
                                        />
                                    })
                                }
                            </Styled.GalleryWrapper>
                        </Styled.ContentWrapper>
                    </Fragment>
                }
            </UserDetailsLoader>
        </Styled.UserGalleryContainer>
    </BaseLayout>
}

export default UserGalleryComponent
