import { connect } from "react-redux"
import UserGalleryComponent from "./component"
import { registerUser, getUserDetailsRequest } from 'application/redux/actions/users'
import { withRouter } from "react-router"
import { showGallery } from "application/redux/actions/gallery"

const mapStateToProps = (state, props) => {
    const { userId } = props.match.params
    const users = state.users
    const userIsLoaded = users.length > 0 && users.filter(u => u.id === userId && u.succeed).length > 0
    const userDetails = userIsLoaded && users.find(u => u.id === userId).payload
    return {
        users,
        userIsLoaded,
        userDetails
    }
}
    


const mapDispatchToProps = dispatch => ({
    getUserDetails: userId => {
        dispatch(getUserDetailsRequest(userId))
    },
    registerUser: userId => {
        dispatch(registerUser(userId))
    },
    openGallery: (fileId, fileName, thumbnails) => dispatch(showGallery(fileId, fileName, thumbnails))
})

export default withRouter(connect(
    mapStateToProps, mapDispatchToProps
)(UserGalleryComponent))