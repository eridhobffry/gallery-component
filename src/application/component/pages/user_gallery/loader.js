import React, { Fragment, useEffect } from 'react'
import { withRouter } from 'react-router'

const UserDetailsLoader = (props) => {
    const { children, match, users, getUserDetails, registerUser } = props
    const userId = match.params.userId
    const userRegistered = users.filter(u => u.id === userId).length > 0
    const userLoaded = userRegistered && users.find(u => u.id === userId).succeed
    const userFailed = userRegistered && users.find(u => u.id === userId).failed
    const userShouldLoad = !userLoaded && !userFailed
    useEffect(() => {
        !userRegistered && registerUser(userId)
    }, [userRegistered, registerUser, userId])
    useEffect(() => {
        userShouldLoad &&  getUserDetails(userId)
    }, [userShouldLoad, getUserDetails, userId])

    
    if (userLoaded) {
        return <Fragment>
            {children}
        </Fragment>
    }
    if (userFailed) {
        return <Fragment>
            User not found
        </Fragment>
    }
    return <Fragment>
        Loading...
    </Fragment>
}

export default withRouter(UserDetailsLoader)
