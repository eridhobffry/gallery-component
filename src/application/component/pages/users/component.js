import ActionButton from 'application/component/controls/action_button'
import { ROUTES } from 'application/constants'
import React from 'react'
import { BaseLayout } from '../_layout'
import * as Styled from './styles'

const UsersComponent = () => {
    return <BaseLayout>
        <Styled.UserContainer>
            <ActionButton
                linkTo={ROUTES.USERS + '/1'}
                secondary
                buttonText={'GALLERY USER 1'}
            />
            <ActionButton
                linkTo={ROUTES.USERS + '/2'}
                secondary
                buttonText={'GALLERY USER 2'}
            />
        </Styled.UserContainer>
    </BaseLayout>
}

export default UsersComponent
