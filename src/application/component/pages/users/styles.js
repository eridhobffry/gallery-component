import styled from "styled-components";

export const UserContainer = styled.div`
    display: flex;
    flex: 1 1 100%;
    flex-flow: row wrap;
    justify-content: space-evenly;
    align-items: center;
`