import ActionButton from 'application/component/controls/action_button'
import { ROUTES } from 'application/constants'
import React, { Fragment } from 'react'
import { BaseLayout } from '../_layout'
import * as Styled from './styles'

const MainComponent = () => {
    return <Fragment>
        <BaseLayout>
            <Styled.MainContainer>
                <ActionButton
                    linkTo={ROUTES.USERS}
                    secondary
                    buttonText={'SEE ALL USERS'}
                />
            </Styled.MainContainer>
        </BaseLayout>
    </Fragment>
}

export default MainComponent
