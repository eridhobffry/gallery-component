import styled from "styled-components";

export const MainContainer = styled.div`
    display: flex;
    flex: 1 1 100%;
    flex-flow: row wrap;
    justify-content: center;
    align-items: center;
`