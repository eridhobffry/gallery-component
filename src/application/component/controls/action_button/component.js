import React from 'react'
import * as Styled from './styles'

const ActionButton = ({
    buttonText,
    className,
    onButtonClick,
    strechted,
    disabled = false,
    secondary = false,
    noBackground = false,
    color = '#fff',
    fontSize = '18px',
    height,
    borderradius,
    linkTo = ''
}) => {
    return <Styled.StyledButton to={linkTo} disabled={disabled} onClick={onButtonClick} strechted={strechted}>
        <Styled.StyledButtonWrapper
            borderradius={borderradius}
            height={height}
            color={color}
            fontSize={fontSize}
            noBackground={noBackground}
            secondary={secondary}
            className={className}
            disabled={disabled}>
            {buttonText}
        </Styled.StyledButtonWrapper>
    </Styled.StyledButton>
}


export default ActionButton