import { NavLink } from "react-router-dom"
import styled from "styled-components"

export const StyledButton = styled(NavLink)`
    padding: 0px;
    margin: 0px;
    /* background-color: ${props => props.theme.color.color0}; */
    border: none;
    cursor: pointer;
    outline: none;
    -webkit-tap-highlight-color: ${props => props.theme.color.color0};
    width:${props => props.strechted ? '100%' : 'auto'};
    text-decoration: none;
`

export const StyledButtonWrapper = styled.div`
    box-sizing: border-box;
    display: flex;
    align-items: center;
    flex-direction: row;
    justify-content: center;
    flex-shrink: 0;
    border-style: solid;
    border-width: 1px;
    position: relative;
    z-index: 0;
    min-height: 0px;
    min-width: 80px;
    border-color: ${props => props.noBackground ? 'transparent' : props.secondary ? props.theme.color.color100 : props.theme.color.primary};
    border-radius: ${props => props.secondary ? props.borderradius ? props.borderradius : '0px' : props.borderradius ? props.borderradius : '5px'};
    height: ${props => props.noBackground ? '0px' : props.height !== undefined ? props.height : '60px'};
    justify-content: center;
    padding: ${props => props.noBackground ? '0px' : '19px 24px'};
    transition: background-color 0.2s ease 0s, border-color 0.2s ease 0s, color 0.2s ease 0s;
    opacity: ${props => props.disabled ? '0.5' : '1'};
    cursor: ${props => props.disabled ? 'default' : 'pointer'};
    background-color: ${props => props.noBackground ? 'transparent' : props.secondary ? props.theme.color.color100 : props.theme.color.primary};
    max-width: 100%;
    color: ${props => props.color};
    font-weight: ${props => props.theme.fontWeight.medium};
    font-size: ${props => props.fontSize};
    line-height: 20px;
    visibility: visible;
    -webkit-font-smoothing: antialiased;
    text-rendering: geometricprecision;
    text-size-adjust: none;

    &::hover {
        background-color: ${props => props.secondary ? props.theme.color.color90 : props.theme.color.primary};
    }
`