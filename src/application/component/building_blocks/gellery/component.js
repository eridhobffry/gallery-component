import React, { Fragment } from 'react'
import * as Styled from './styles'
import ThumbnailSlider from 'application/component/building_blocks/thumbnails_slider'
import FullAttachment from 'application/component/building_blocks/full_attachment'

const GalleryComponent = (props) => {
    const { gallery, onSelectAttachment, onCloseGallery } = props
    const handleSelectAttachment = (att) => {
        onSelectAttachment(att.id, att.name)
    }
    return gallery.isVisible ? <Styled.StyledOverlayDimmer isVisible={gallery.isVisible}>
        <Styled.StyledOverlayContent>
            <Styled.StyledHeaderRow>
                <Styled.StyledHeader>
                    <Styled.StyledSidebarHeaderCaption>
                        {gallery.selectedName}
                    </Styled.StyledSidebarHeaderCaption>
                    <Styled.StyledSidebarHeaderCloseButton>
                        <i style={{cursor: 'pointer'}} onClick={onCloseGallery} className='material-icons'>clear</i>
                    </Styled.StyledSidebarHeaderCloseButton>
                </Styled.StyledHeader>
            </Styled.StyledHeaderRow>
            <Styled.StyledContent>
                <FullAttachment
                    fileId={gallery.selectedId}
                />
                {
                    gallery.thumbnails.length > 0 && <Styled.StyledFooterRow>
                        <ThumbnailSlider
                            thumbnailsForSlider={gallery.thumbnails}
                            onThumbnailClick={(att) => handleSelectAttachment(att)}
                            selectedThumbnailId={gallery.selectedId}
                        />
                    </Styled.StyledFooterRow>
                }
            </Styled.StyledContent>
        </Styled.StyledOverlayContent>
    </Styled.StyledOverlayDimmer> : <Fragment/>
}

export default GalleryComponent
