import styled from 'styled-components'

export const StyledOverlayDimmer = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 6000;
    background-color: rgba(0, 0, 0, 0.55);
    display: ${props => props.isVisible ? 'block' : 'none'}
`

export const StyledOverlayContent = styled.div`
    position: relative;
    top: 16px;
    left: 16px;
    width: 100%;
    width: calc(100% - 32px);
    height: 100%;
    height: calc(100% - 32px);
    z-index: 6001;
    background-color: ${props => props.theme.color.color0};
    display: flex;
    flex-flow: row wrap;
`

export const StyledHeader = styled.h2`
    margin-top: 0;
    align-items: center;
    padding: 10px;
    display: flex;
    flex-direction: row;
    max-width: calc(100% - 20px);
    font-size: ${props => props.theme.fontSize.headline2};
    font-weight: ${props => props.theme.fontWeight.light};
`

export const StyledSidebarHeaderCaption = styled.div`
    flex: 1;
    overflow: hidden;
`

export const StyledSidebarHeaderCaptionOnQueue = styled.div`
    flex: 1;
    overflow: hidden;
    filter: blur(${props => props.blurred ? '10px' : '0'});
`

export const StyledSidebarHeaderCloseButton = styled.div`
    flex: 0 0 30px;
`

export const StyledSidebarHeaderDownloadButton = styled.div`
    flex: 0 0 30px;
`

export const StyledSendButtonOnQueue = styled.div`
    flex: 0 0 30px;
    padding: 20px;
`

export const StyledHeaderRow = styled.div`
    height: 50px;
    flex: 0 0 100%;
    max-width: 100%;
`

export const StyledFooterRow = styled.div`
    height: 100px;
    background: #eeeeee;
    flex: 0 0 100%;
    max-width: 100%;
`

export const StyledContent = styled.div`
    left: 0; 
    right: 0;
    bottom: 100px;
    width: 100%;
    height: calc(100% - 150px);
    text-align: center;
`

export const StyledContentonQueue = styled.div`
    left: 0; 
    right: 0;
    bottom: 100px;
    width: 100%;
    height: calc(100% - 280px);
    filter: blur(${props => props.blurred ? '10px' : '0'});
    text-align: center;
`