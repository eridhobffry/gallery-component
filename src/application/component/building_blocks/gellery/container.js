import { hideGallery, setActiveFile } from "application/redux/actions/gallery"
import { connect } from "react-redux"
import GalleryComponent from "./component"

const mapStateToProps = (state) => ({
    gallery: state.gallery,
})

const mapDispatchToProps = dispatch => ({
    onSelectAttachment: (fileId, fileName) => dispatch(setActiveFile(fileId, fileName)),
    onCloseGallery: () => dispatch(hideGallery())
})

export default connect(
    mapStateToProps, mapDispatchToProps
)(GalleryComponent)
