import React, { useRef, useEffect } from 'react'
import styled from 'styled-components'
import Slider from 'react-slick'
import Thumbnail from 'application/component/building_blocks/thumbnails'
import { nanoid } from 'nanoid'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const StyledSlider = styled.div`
    max-width: calc(100% - 100px);
    margin: auto;
    & .slick-arrow:before{
        color: ${props => props.theme.color.color70}
    }
`

const ThumbnailSlider = ({
    thumbnailsForSlider,
    onThumbnailClick,
    selectedThumbnailId,
}) => {
    const sliderSettings = {
        dots: false,
        infinite: false, //weird behavior if used with effect
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true, //bit buggy?
        variableWidth: true,
        centerPadding: '0'
    }

    const slider = useRef(null)

    useEffect(() => { //scroll to selected thumbnail on init
        var currentIndex = 0
        if (thumbnailsForSlider.length > 1) {
            currentIndex = thumbnailsForSlider.map(x => x.id).indexOf(selectedThumbnailId)
            slider.current.slickGoTo(currentIndex)
        }
    }, [selectedThumbnailId, thumbnailsForSlider])

    return thumbnailsForSlider.length > 1 && <StyledSlider>
        <Slider ref={slider} {...sliderSettings}>
            {
                thumbnailsForSlider.map((thumbnail) => {
                    return <Thumbnail
                        key={nanoid()}
                        fileId={thumbnail.id}
                        fileName={thumbnail.name}
                        onThumbnailClick={onThumbnailClick}
                        isActive={thumbnail.id === selectedThumbnailId}
                    />
                })
            }
        </Slider>
    </StyledSlider>
}

export default ThumbnailSlider