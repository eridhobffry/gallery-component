import { connect } from "react-redux"
import ThumbnailSlider from "./component"

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(ThumbnailSlider)