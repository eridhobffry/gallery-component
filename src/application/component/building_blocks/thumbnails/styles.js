import styled from 'styled-components'

export const ThumbnailWrapper = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    text-align: center;
    margin-right: ${props => props.marginRight ? '20px' : '0px'};
    padding: 5px 10px 11px 10px;
    background: ${props => props.isActive ? props.theme.color.color10 : 'transparent'};
    border-radius: 2px;
`

export const StyledCaption = styled.div`
    font-size: ${props => props.theme.fontSize.small};
    font-weight: ${props => props.theme.fontWeight.light};
    margin: 0;
    max-width: 100px;
`

export const Thumbnail = styled.div`
    display: flex;
    align-items: center;
    align-content: center;
    height: ${props => props.isSizeFixed ? props.divHeight ? props.divHeight : '70px' : `68px`};
    width: ${props => props.isSizeFixed ? props.divWidth ? props.divWidth : '70px' : `62px`};
    background: ${props => props.isSizeFixed ? `url(${props.imageSrc})` : 'unset'};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    justify-content: center;
    &:hover>.overlay{
        display:flex;
    }
`

export const ThumbnailImage = styled.img`
    max-height: 68px;
    max-width: 62px;
    width: auto;
    height: auto;
    border: 1px solid ${props => props.isActive ? props.theme.color.primary : props.theme.color.color15};
    outline: 3px solid ${props => props.isActive ? props.theme.color.primary : 'transparent'}
`
