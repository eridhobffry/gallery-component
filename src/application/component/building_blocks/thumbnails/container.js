import { connect } from "react-redux"
import ThumbnailComponent from "./component"
import { registerThumbnail, getThumbnailRequest } from 'application/redux/actions/files'

const mapStateToProps = (state, props) => {
    const thumbnails = state.files.thumbnails
    const thumbnailRegistered = thumbnails.length > 0 && thumbnails.filter(f => f.id === props.fileId).length > 0
    const thumbnailLoaded = thumbnailRegistered && thumbnails.filter(f => f.id === props.fileId && f.succeed).length > 0
    const thumbnailDetails = thumbnailLoaded && thumbnails.find(f => f.id === props.fileId)
    return {
        thumbnails,
        thumbnailRegistered,
        thumbnailLoaded,
        thumbnailDetails
    }
}

const mapDispatchToProps = dispatch => ({
    registerThumbnail: fileId => dispatch(registerThumbnail(fileId)),
    getThumbnailByIdRequest: fileId => dispatch(getThumbnailRequest(fileId)),
})

export default connect(
    mapStateToProps, mapDispatchToProps
)(ThumbnailComponent)
