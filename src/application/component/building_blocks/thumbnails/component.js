import React, { Fragment } from 'react'
import ThumbnailLoader from './loader'
import * as Styled from './styles'

const ThumbnailComponent = (props) => {
    const { thumbnailLoaded, thumbnailDetails, onThumbnailClick = () => {}, isActive = false } = props
    const handleThumbnailClick = () => {
        onThumbnailClick(thumbnailDetails)
    }
    return <ThumbnailLoader {...props}>
        {
            thumbnailLoaded && <Fragment>
                <Styled.ThumbnailWrapper isActive={isActive} onClick={handleThumbnailClick}>
                    <Styled.ThumbnailImage isActive={isActive} src={thumbnailDetails.data} alt={thumbnailDetails.id}/>
                </Styled.ThumbnailWrapper>
            </Fragment>
        }
    </ThumbnailLoader>
}

export default ThumbnailComponent
