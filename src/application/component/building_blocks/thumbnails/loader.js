import React, { Fragment, useEffect } from 'react'

const ThumbnailLoader = (props) => {
    const { thumbnailRegistered, thumbnailLoaded, registerThumbnail, getThumbnailByIdRequest, fileId, children } = props
    useEffect(() => {
        !thumbnailRegistered && registerThumbnail(fileId)
    }, [thumbnailRegistered, registerThumbnail, fileId])
    useEffect(() => {
        !thumbnailLoaded && getThumbnailByIdRequest(fileId)
    }, [thumbnailLoaded, getThumbnailByIdRequest, fileId])

    return <Fragment>
        {children}
    </Fragment>
}

export default ThumbnailLoader
