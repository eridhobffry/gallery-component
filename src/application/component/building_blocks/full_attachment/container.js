import { connect } from "react-redux"
import { registerFullAttachment, getFullAttachmentRequest } from 'application/redux/actions/files'
import FullAttachmentComponent from "./component"

const mapStateToProps = (state, props) => {
    const fullAttachments = state.files.fullAttachments
    const fullAttachmentRegistered = fullAttachments.length > 0 && fullAttachments.filter(f => f.id === props.fileId).length > 0
    const fullAttachmentLoaded = fullAttachmentRegistered && fullAttachments.filter(f => f.id === props.fileId && f.succeed).length > 0
    const fullAttachmentDetails = fullAttachmentLoaded && fullAttachments.find(f => f.id === props.fileId)
    return {
        fullAttachments,
        fullAttachmentRegistered,
        fullAttachmentLoaded,
        fullAttachmentDetails
    }
}

const mapDispatchToProps = dispatch => ({
    registerFullAttachment: fileId => dispatch(registerFullAttachment(fileId)),
    getFullAttachmentRequest: fileId => dispatch(getFullAttachmentRequest(fileId)),
})

export default connect(
    mapStateToProps, mapDispatchToProps
)(FullAttachmentComponent)
