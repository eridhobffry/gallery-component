import React from 'react'
import FullAttachmentLoader from './loader'
import * as Styled from './styles'

const FullAttachmentComponent = (props) => {
    const { fullAttachmentLoaded, fullAttachmentDetails } = props
    return <FullAttachmentLoader {...props}>
        {
            fullAttachmentLoaded &&
            <Styled.FullAttachmentImage src={fullAttachmentDetails.data} alt={fullAttachmentDetails.id} />
        }
    </FullAttachmentLoader>
}

export default FullAttachmentComponent
