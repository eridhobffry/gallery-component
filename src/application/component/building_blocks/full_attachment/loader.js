import React, { Fragment, useEffect } from 'react'

const FullAttachmentLoader = (props) => {
    const { fullAttachmentRegistered, fullAttachmentLoaded, registerFullAttachment, getFullAttachmentRequest, fileId, children } = props
    useEffect(() => {
        !fullAttachmentRegistered && registerFullAttachment(fileId)
    }, [fullAttachmentRegistered, registerFullAttachment, fileId])
    useEffect(() => {
        !fullAttachmentLoaded && getFullAttachmentRequest(fileId)
    }, [fullAttachmentLoaded, getFullAttachmentRequest, fileId])

    return <Fragment>
        {children}
    </Fragment>
}

export default FullAttachmentLoader
