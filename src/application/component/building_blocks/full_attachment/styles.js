import styled from 'styled-components'

export const FullAttachmentImage = styled.img`
    width: auto;
    max-width: 80%;
`
