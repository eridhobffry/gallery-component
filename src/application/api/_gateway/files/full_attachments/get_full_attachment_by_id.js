import { fetchHandler, fetchRequestBuilder, HTTP_METHOD, limiter } from "../../../request_builders"
import { handleApiResponse } from "../../../response_handlers"

export const getFullFilesById = (fileId) => {
    const request = fetchRequestBuilder(`http://localhost:4000/attachments/${fileId}`, HTTP_METHOD.GET)

    return limiter.schedule(() => fetchHandler(request.url, request.data))
        .then(handleApiResponse)
}