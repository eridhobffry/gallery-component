import * as thumbnails from './thumbnails'
import * as fullAttachments from './full_attachments'
export {
    thumbnails,
    fullAttachments
}