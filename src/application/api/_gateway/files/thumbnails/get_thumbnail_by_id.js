import { handleApiResponse } from "application/api/response_handlers"
import { fetchHandler, fetchRequestBuilder, HTTP_METHOD, limiter } from "../../../request_builders"

export const getThumbnailsById = (fileId) => {
    const request = fetchRequestBuilder(`http://localhost:4000/thumbnails/${fileId}`, HTTP_METHOD.GET)

    return limiter.schedule(() => fetchHandler(request.url, request.data))
        .then(handleApiResponse)
}