const { nanoid } = require("nanoid")

module.exports = () => {
    return {
        addresses: {},
        createdAt: '2020-06-18T15:17:51',
        email: 'santi.muster@rollian.com',
        firstName: 'Santi',
        id: nanoid(),
        lastName: 'Muster',
        modifiedAt: '2020-06-18T15:17:51',
        salutation: 'Frau',
        gallery: [
            {
                id: 1
            },
            {
                id: 4
            },
            {
                id: 5
            },
            {
                id: 3
            }
        ]
    }
}