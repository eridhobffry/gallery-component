const { nanoid } = require("nanoid")

module.exports = () => {
    return {
        addresses: {},
        createdAt: '2020-06-18T15:17:51',
        email: 'john.doe@rollian.com',
        firstName: 'John',
        id: nanoid(),
        lastName: 'Doe',
        modifiedAt: '2020-06-18T15:17:51',
        salutation: 'Herr',
        gallery: [
            {
                id: 1
            },
            {
                id: 3
            },
            {
                id: 6
            },
            {
                id: 2
            }
        ]
    }
}