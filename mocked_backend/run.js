const jsonServer = require('json-server')
const cookieParser = require('cookie-parser')

const server = jsonServer.create()
const middlewares = jsonServer.defaults()
const port = process.env.PORT || 4000

server.use(cookieParser('MYCOOKIES'))
server.use(jsonServer.bodyParser)
server.use(middlewares)

function getRandomInt(min, max) {
	min = Math.ceil(min)
	max = Math.floor(max)
	return Math.floor(Math.random() * (max - min + 1)) + min
}

//mock server delay:
server.use(function (req, res, next) {
	var del = getRandomInt(100, 400)
	console.log('### using random delay of ' + del + ' ms')
	setTimeout(next, del)
})

server.use(function (req, res, next) {
	res.header('Access-Control-Allow-Credentials', true)
	res.header('Access-Control-Allow-Origin', req.headers.origin)
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,UPDATE,OPTIONS')
	res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept')
	next()
})

server.get('/attachments/1', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/dominik')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/attachments/2', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/jared-rice')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/attachments/3', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/javardh')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/attachments/4', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/ken-cheung')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/attachments/5', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/kristopher')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/attachments/6', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/full_attachments/marek')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/1', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/dominik_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/2', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/jared-rice_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/3', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/javardh_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/4', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/ken-cheung_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/5', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/kristopher_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/thumbnails/6', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./files/thumbnails/marek_thumbnail')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/user/details/1', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./users/1')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.get('/user/details/2', (request, response) => {
	if (request.method === 'GET') {
		const attachmentDetails = require('./users/2')
		response.status(200).jsonp(attachmentDetails())
	}
})

server.listen(port, () => {
	console.log('JSON Server is running')
})